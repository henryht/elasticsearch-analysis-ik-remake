package org.wltea.analyzer.util;

import java.io.FileInputStream;
import java.nio.file.Path;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.common.io.PathUtils;
import org.elasticsearch.plugin.analysis.ik.AnalysisIkPlugin;
import org.wltea.analyzer.cfg.Configuration;
import org.wltea.analyzer.help.ESPluginLoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

/**
 * 数据库连接工具
 */
public class JdbcUtil {

    private static final Logger logger = ESPluginLoggerFactory.getLogger(JdbcUtil.class.getName());

    public static final String SQL_FILE_NAME = "jdbc-reload.properties";
    /**
     * 数据库连接
     */
    private static String url;
    /**
     * 数据库账号
     */
    private static String user;
    /**
     * 数据库密码
     */
    private static String password;
    /**
     * 每次执行更新的间隔时间
     */
    private static Long interval;
    /**
     * 扩展词SQL
     */
    private static String extWordSql;
    /**
     * 停词SQL
     */
    private static String stopWordSql;

    /**
     * 初始化jdbc参数
     */
    public static synchronized void init(Configuration cfg) {
        Path path = cfg.getConfigInPluginDir();
        Path file = PathUtils.get(path.toAbsolutePath().toString(), SQL_FILE_NAME);
        InputStream in = null;
        try {
            in = new FileInputStream(file.toFile());
            Properties props = new Properties();
            props.load(in);
            url = props.getProperty("jdbc.url");
            user = props.getProperty("jdbc.user");
            password = props.getProperty("jdbc.password");
            interval = Long.valueOf(props.getProperty("jdbc.reload.interval"));
            extWordSql = props.getProperty("jdbc.reload.ext.word.sql");
            stopWordSql = props.getProperty("jdbc.reload.stop.word.sql");
            in.close();
        } catch (IOException e) {
            logger.error("error", e);
            logger.info("JdbcUtil加载配置文件异常！");
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException e) {
                logger.error("error", e);
            }
        }
    }

    /**
     * 获取连接
     */
    public static Connection getConnection() {
        try {
            return DriverManager.getConnection(url, user, password);
        } catch (Exception e) {
            throw new RuntimeException("连接数据库异常，请检查您的配置文件");
        }
    }

    /**
     * 释放资源
     */
    public static void release(ResultSet rs, Statement stmt, Connection conn) {
        if (rs != null) {
            try {
                rs.close();//将结果集中关闭
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        //如果处理Sql语句对象不为空
        if (stmt != null) {
            try {
                stmt.close();//将处理Sql语句对象关闭
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        //如果 连接不为空
        if (conn != null) {
            try {
                conn.close();//将连接关闭
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getExtWordSql() {
        return extWordSql;
    }

    public static String getStopWordSql() {
        return stopWordSql;
    }

    public static long getInterval() {
        return interval;
    }
}
